const fs = require('fs')
const athleteData = require('../data/athlete_events.json');
const regionData=require('../data/noc_regions.json');
const functions = require('./olympics.js')

const jsonData={
hostingsPerCity:functions.hostingsPerCity(athleteData),
mostMedalWiningCountry:functions.mostMedalWiningCountry(athleteData,regionData),
genderStatisticsDecadeWise:functions.genderStatisticsDecadeWise(athleteData),
averageAgePerYear:functions.averageAgePerYear(athleteData),
indianMedalWinners:functions.indianMedalWinners(athleteData),

}

fs.writeFile('../output/output.json',
JSON.stringify(jsonData,null,5),
err=>{
    // console.log('Complete');
    if(err){
        console.log(err);
    }
});


