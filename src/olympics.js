const athleteData = require('../data/athlete_events.json');
const regionData=require('../data/noc_regions.json');

//Number of times olympics hosted per City over the years - Pie chart
//module.exports.hostingsPerCity=(data)=>{
    // const noOfTimesHosted=data.reduce((record,obj)=>{
    //     if(!record.hasOwnProperty(obj['Games'])){
    //         record[obj['Games']]='Season';
    //         if(record.hasOwnProperty(obj['City'])){
    //             record[obj['City']]++;
    //         }else{
    //         record[obj['City']]=1;
    //         }
    //     }
    //     // console.log(record);
    // return record;
    // },{});
    // // console.log(noOfTimesHosted);
    // let noOfTimesHostedInToArray= Object.keys(noOfTimesHosted).map(obj=>
    // {
    //     return {[obj]:noOfTimesHosted[obj]
    //     };
    // });
    // let result=noOfTimesHostedInToArray.filter(obj=>{
    //     return Object.values(obj)!='Season';
    // });
    // let final=Object.assign({},...result);
    // return final;
//}
// console.log(hostingsPerCity(athleteData));

function mapvalues(obj,fun){
    return Object.keys(obj).reduce(function(record,key){
        record[key] = fun(obj[key]);
        return record;
    },{})
};

module.exports.hostingsPerCity=(data)=>{
    const hostedCities=athleteData.reduce((record,obj)=>{
        if(record.hasOwnProperty(obj['City'])){
            record[obj['City']].add(obj['Games']);
        }else{
            record[obj['City']]= new Set();
            record[obj['City']].add(obj['Games']);
        }
    return record;
    },{});
    return mapvalues(hostedCities,(obj)=>{
        return obj.size;
    })
}



//Top 10 countries who have won most medals after 2000 - stacked column - split gold/silver/bronze
module.exports.mostMedalWiningCountry=(data,regionData)=>{  
    const medals=data.reduce((record,obj)=>{
        if(obj['Medal']!='NA'&& obj['Year']>2000){
            if(record.hasOwnProperty(obj['NOC'])){
                if(record[obj['NOC']].hasOwnProperty(obj['Medal'])){
                record[obj['NOC']][obj['Medal']]++;
                record[obj['NOC']].count++;
                }
                else{
                    record[obj['NOC']][obj['Medal']]=1;
                    record[obj['NOC']].count++;
                }
            }else{
                record[obj['NOC']]={};
                record[obj['NOC']].count=1;
                record[obj['NOC']][obj['Medal']]=1;
            }
        }
    return record;
    },{});
        let topTen={};
        let sorted = Object.keys(medals).sort((a, b) => medals[b].count - medals[a].count);
        let sliced=sorted.slice(0,10);
        sliced.forEach(function(key) {
            delete medals[key].count;
            topTen[key] = medals[key];
        });
        let countryName=regionData.reduce((record,obj)=>{
            record[obj['NOC']]=obj['region'];
            return record;
        },{});
        let topTenCountries=Object.keys(topTen).reduce((record,obj)=>{
            record.push({[countryName[obj]]:topTen[obj]});
            return record;
        },[]);
        // let topTenMedalWinningCountries=Object.assign([], ...topTenCountries);
        // console.log(topTenMedalWinningCountries);
    return topTenCountries;
}

//M/F participation by decade - column chart
// module.exports.genderStatisticsDecadeWise=(data)=>{
//     const participations=data.reduce((record,obj)=>{
//         if(obj['Sex']!='NA'){
//             if(record.hasOwnProperty(obj['Year'])){
//                 if(record[obj['Year']]['Name'].hasOwnProperty(obj['ID'])){
//                     if(record[obj['Year']]['Name'][obj['ID']]!=obj['Games']){
//                         record[obj['Year']][obj['Sex']]++;
//                         record[obj['Year']]['Name'][obj['ID']]=obj['Games'];
//                     }                    
//                 }else{
//                     record[obj['Year']][obj['Sex']]++;
//                     record[obj['Year']]['Name'][obj['ID']]=obj['Games'];
//                 }                
//             }else{
//                 record[obj['Year']]={};
//                 record[obj['Year']]['M']=0;
//                 record[obj['Year']]['F']=0;
//                 record[obj['Year']][obj['Sex']]=1;
//                 record[obj['Year']]['Name']={};
//                 record[obj['Year']]['Name'][obj['ID']]=obj['Games'];
//             }
//         }  
//         return record;
//     },{});
//     Object.keys(participations).map((key)=>{
//         delete participations[key]['Name'];
//     })

//     const decadeParticipation=Object.keys(participations).reduce((record,key)=>{
//         let startYear=(parseInt(key/10)*10);
//         let endYear=((parseInt(key/10)+1)*10)-1;
//             if(record.hasOwnProperty(startYear+'-'+(endYear))){
//                 record[startYear+'-'+(endYear)]['M']+=participations[key]['M'];
//                 record[startYear+'-'+(endYear)]['F']+=participations[key]['F'];
//             }else{
//                 record[startYear+'-'+(endYear)]={};
//                 record[startYear+'-'+(endYear)]['M']=participations[key]['M'];
//                 record[startYear+'-'+(endYear)]['F']=participations[key]['F'];
//             }
            
//         return record;
//     },{});
//     console.log(decadeParticipation);
//     return decadeParticipation;
// }
// genderStatisticsDecadeWise(athleteData);

module.exports.genderStatisticsDecadeWise=(data)=>{
const participations=athleteData.reduce((record,obj)=>{
        if(obj['Sex']!='NA'){
            if(record.hasOwnProperty(obj['Year'])){
                record[obj['Year']][obj['Sex']].add(obj['ID']);
            }else{
                record[obj['Year']]={};
                record[obj['Year']]['M']=new Set();
                record[obj['Year']]['F']=new Set();
                record[obj['Year']][obj['Sex']].add(obj['ID']);
            }
        }  
        return record;
    },{});
    // console.log(participations);
    const yearParticipations= mapvalues(participations,(obj)=>{
        return Object.assign({},...Object.keys(obj).map((key) => {
            return {[key] : obj[key].size};
        },{}))
    });
    const decadeParticipation=Object.keys(yearParticipations).reduce((record,key)=>{
        let startYear=(parseInt(key/10)*10);
        let endYear=((parseInt(key/10)+1)*10)-1;
            if(record.hasOwnProperty(startYear+'-'+(endYear))){
                record[startYear+'-'+(endYear)]['M']+=yearParticipations[key]['M'];
                record[startYear+'-'+(endYear)]['F']+=yearParticipations[key]['F'];
            }else{
                record[startYear+'-'+(endYear)]={};
                record[startYear+'-'+(endYear)]['M']=yearParticipations[key]['M'];
                record[startYear+'-'+(endYear)]['F']=yearParticipations[key]['F'];
            }
        return record;
    },{});
    // console.log(decadeParticipation);
    return decadeParticipation;
}

//Per year average age of athletes who participated in Boxing Men’s Heavyweight - Line
module.exports.averageAgePerYear=(data)=>{
    const avgAge=data.reduce((record,obj)=>{
        if(obj['Event']=="Boxing Men's Heavyweight"&& obj['Age']!='NA'){
            if(record.hasOwnProperty(obj['Year'])){
                record[obj['Year']]['totalAge']+=parseInt(obj['Age']);
                record[obj['Year']].count++;
            }else{
                record[obj['Year']]={};
                record[obj['Year']]['totalAge']=parseInt(obj['Age']);
                record[obj['Year']].count=1;
            }
        }
        return record;
    },{});
    Object.keys(avgAge).map((key)=>{
        avgAge[key]=parseFloat(avgAge[key]['totalAge']/avgAge[key].count).toFixed(2);
    })
    Object.keys(avgAge).sort();
    return avgAge;
}

//Find out all medal winners from India per season - Table
module.exports.indianMedalWinners= (data)=>{
    const medalWinners=data.reduce((record,obj)=>{
        if(obj['NOC']=='IND'&& obj['Medal']!='NA'){
            if(record.hasOwnProperty(obj['Season'])){
                record[obj['Season']].push(obj);
            }else{
                record[obj['Season']]=[obj];
            }
        }
        return record;
    },{});
    return medalWinners;
}

//find medal winners of germany in 2000
function germanyMedalWinners(data){
    let medalWinners=data.filter(obj=>{
        return obj['NOC']=='GER'&&obj['Medal']!=='NA'&&obj['Year']==2000;
    }).reduce((record,obj)=>{
           if(record.hasOwnProperty(obj['Medal'])){
                record[obj['Medal']].push(obj);
            }else{
                record[obj['Medal']]=[obj];
            }
       
        return record;
    },{});

    return medalWinners;
}
// console.log(germanyMedalWinners(athleteData));
